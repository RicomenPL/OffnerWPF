﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace nauka
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
            

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (off.IsChecked == true)
            {
                if (sec.IsChecked == true)
                {
                    string number = amount.Text;
                    string shut = "/s /t " + number;
                    Process.Start("shutdown", shut);
                }
                else if (min.IsChecked == true)
                {
                    int number = int.Parse(amount.Text);
                    number = number * 60;
                    string shut = "/s /t " + number.ToString();
                    Process.Start("shutdown", shut);
                    MessageBox.Show("Komputer zostanie wyłączony");
                }
                else if (h.IsChecked == true)
                {
                    int number = int.Parse(amount.Text);
                    number = number * 3600;
                    string shut = "/s /t " + number.ToString();
                    Process.Start("shutdown", shut);
                    MessageBox.Show("Komputer zostanie wyłączony");
                }
                else if (days.IsChecked == true)
                {
                    int number = int.Parse(amount.Text);
                    number = number * 86400;
                    string shut = "/s /t " + number.ToString();
                    Process.Start("shutdown", shut);
                    MessageBox.Show("Komputer zostanie wyłączony");
                }

            }
            else if (restart.IsChecked == true)
            {
                if (sec.IsChecked == true)
                {
                    string number = amount.Text;
                    string shut = "/r /t " + number;
                    Process.Start("shutdown", shut);
                    MessageBox.Show("Komputer zostanie zrestartowany");
                }
                else if (min.IsChecked == true)
                {
                    int number = int.Parse(amount.Text);
                    number = number * 60;
                    string shut = "/r /t " + number.ToString();
                    Process.Start("shutdown", shut);
                    MessageBox.Show("Komputer zostanie zrestartowany");
                }
                else if (h.IsChecked == true)
                {
                    int number = int.Parse(amount.Text);
                    number = number * 3600;
                    string shut = "/r /t " + number.ToString();
                    Process.Start("shutdown", shut);
                    MessageBox.Show("Komputer zostanie zrestartowany");
                }
                else if (days.IsChecked == true)
                {
                    int number = int.Parse(amount.Text);
                    number = number * 86400;
                    string shut = "/r /t " + number.ToString();
                    Process.Start("shutdown", shut);
                    MessageBox.Show("Komputer zostanie zrestartowany");
                }
            }
        

    }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            amount.Text = String.Empty;
            off.IsChecked = true;
            sec.IsChecked = true;
        }
    }
}
